package com.demo.developer.deraesw.demomoviewes.network.response

import com.demo.developer.deraesw.demomoviewes.data.entity.MovieGenre

class MovieGenreResponse {
    val genres : List<MovieGenre> = ArrayList()
}