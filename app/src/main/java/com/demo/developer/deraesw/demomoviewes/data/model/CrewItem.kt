package com.demo.developer.deraesw.demomoviewes.data.model

data class CrewItem(var id : Int = 0,
               var name : String = "",
               var profilePath : String  = "",
               var department : String = "",
               var job : String = "") {
}