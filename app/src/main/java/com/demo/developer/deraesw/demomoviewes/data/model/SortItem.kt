package com.demo.developer.deraesw.demomoviewes.data.model

data class SortItem (val key : String, val title : String = "", var selected : Boolean = false) {
}