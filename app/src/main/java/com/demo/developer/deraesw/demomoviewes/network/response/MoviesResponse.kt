package com.demo.developer.deraesw.demomoviewes.network.response

import com.demo.developer.deraesw.demomoviewes.data.entity.Movie

class MoviesResponse {
    val page : Int = 0
    val results : List<Movie> = ArrayList()
}